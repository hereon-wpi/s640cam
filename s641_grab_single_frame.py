import ctypes
import cv2
import numpy as np
import time
from math import floor
from egrabber import *
import egrabber as eg
import pdb

############################# buffer properties ############################
# Sets BPC
bpc=1
# Sets number of buffers allocated
numBuff=1
############################################################################
#pdb.set_trace()
############################## image properties ############################
# Bit depth. We use 8 bits per pixel, meaning 2^8 values from 0-255.
whatisBit=8
if whatisBit==8:
    daType=np.uint8
    daTypeMax=255
else:
    daType=np.uint16
    daTypeMax=(2**16)-1
# Mono or color? BayerGB is the color string, Mono is the Mono string.
clrString='Mono'
# Width
w=2560
# Height
h=1600
# Frame rate. Using the string 'max' will max it out for a given resolution.
whatFPS=100
# Exposure time. Using the string 'max' will max it out for a given frame rate.
exptime='max'
############################################################################

############################## initialization ##############################
# Initialize the frame grabber and use the discovery routine to find the 
# hardware, including the camera
gentl=EGenTL()
discovery=EGrabberDiscovery(gentl)
discovery.discover()

# Initialize each bank as separate
bank1=discovery.egrabbers[0]
bankA=EGrabber(bank1)
bank2=discovery.egrabbers[1]
bankB=EGrabber(bank2)
#bank3=discovery.egrabbers[2]
#bankC=EGrabber(bank3)
#bank4=discovery.egrabbers[3]
#bankD=EGrabber(bank4)

# Initialize the camera as a single object
cam=discovery.cameras[0]
grabber=EGrabber(cam)

grabber.remote.set('Banks','Banks_AB')

# Set color format and bit depth. Pixel format = Mono8 as defined above
grabber.remote.set('PixelFormat',clrString+str(whatisBit))

# Set height. The reason for halving is because each bank will handle
# half of the total height.
grabber.remote.set('Height', int(h/2))

# Set width.
grabber.remote.set('Width', w)

# Set FPS.
if whatFPS=='max':
    whatFPS=grabber.remote.get('AcquisitionFrameRate.Max')
grabber.remote.set('AcquisitionFrameRate', whatFPS)

# Set exposure time.
if exptime=='max':
    exptime=grabber.remote.get('ExposureTime.Max')
grabber.remote.set('ExposureTime', exptime)


# Since we are performing a pixel-wise FFC, we don't need to do the
# column-wise FFC. We can turn it off.
grabber.remote.set('FlatFieldCorrection','FlatFieldCorrectionOff')
############################################################################

########################## Important Definitions ###########################
# This will access the buffer and get important information, including...
def grab_frame_info(buffer):
    # The buffer pointer (address to where the frame is)
    bufferPtr = buffer.get_info(BUFFER_INFO_BASE, INFO_DATATYPE_PTR)
    # The image size
    imageSize = buffer.get_info(BUFFER_INFO_CUSTOM_PART_SIZE, 
                                INFO_DATATYPE_SIZET)
    # Number of partitions (retrieves BPC)
    partNum = buffer.get_info(BUFFER_INFO_CUSTOM_NUM_PARTS, 
                              INFO_DATATYPE_SIZET)
    # Timestamp
    timeStamp = buffer.get_info(BUFFER_INFO_TIMESTAMP,INFO_DATATYPE_UINT64)
    
    return bufferPtr, imageSize, partNum, timeStamp
    
# This is your "RAM offloading" routine. It will pre-allocate an empty
# numpy array in memory that has the same height, width, and data type
# as your frame, then it will offload the data from the buffer pool and into
# the numpy array. 
# Importantly, notice how "partNum" comes into play here. If BPC > 1, then
# the multiple frames will be stitched VERTICALLY on top of each other.
# For example, if BPC=2, the first frame will span from (0,0) to (h-1,w-1) and
# the second frame will span from (h,0) to (2*h-1,w-1) in the numpy array.     
def move_frame_from_pool(bufferPtr,imageSize,partNum):
    # Pre-allocate numpy array
    frame = np.empty((partNum*h,w),dtype=daType)
    # Get pointer to address of numpy array
    frame_ptr = frame.ctypes.data_as(ctypes.c_void_p)
    # Move the data from the buffer pointer and to the numpy array pointer
    ctypes.memmove(frame_ptr, bufferPtr, imageSize*partNum)
    return frame

# This is the routine to obtain the black reference.
def get_black_ref(bpc):
    # First, close the shutter and wait two seconds.
    print('Beginning the black reference!')
    grabber.remote.set('LensShutter','LensShutterClose')
    time.sleep(2)
    
    # Temporarily set the BPC back to 1 and allocate a single buffer. We
    # only need one frame. Then, start the frame grabber.
    bankA.stream.set('BufferPartCount',1)
    bankB.stream.set('BufferPartCount',1)
    #bankC.stream.set('BufferPartCount',1)
    #bankD.stream.set('BufferPartCount',1)
    grabber.realloc_buffers(1)
    grabber.start()
    #pdb.set_trace()    
    # We access the buffer with a 'with' block.
    #test=Buffer(grabber)
    with eg.Buffer(grabber) as buffer:
        # Obtain the buffer pointer and image size.
        bufferPtr,imageSize,*_=grab_frame_info(buffer)
        # Our black reference is now offloaded to the numpy array.
        black_ref=move_frame_from_pool(bufferPtr,imageSize,1)
        
    # If in color, debayer the frame.    
    if clrString=='BayerGB':
        black_ref=cv2.cvtColor(black_ref, cv2.COLOR_BayerGB2RGB)
    # If in 12 bpp mode, bit shift 4 times (since the pixel values are sent as
    # 16 bits) and convert to an int32 array. This is to prevent underflow
    # when subtracting out the black reference, as we will see in a bit.
    if whatisBit==12:
        black_ref = black_ref << 4
        black_ref = black_ref.astype(np.int32)
    # If in 8 bpp mode, the black reference only needs to be int16.
    else:
        black_ref = black_ref.astype(np.int16)
    
    # Stop the frame grabber.
    grabber.stop()
    
    # Reset the bpc back to what we had before.
    bankA.stream.set('BufferPartCount',bpc)
    bankB.stream.set('BufferPartCount',bpc)
    #bankC.stream.set('BufferPartCount',bpc)
    #bankD.stream.set('BufferPartCount',bpc)
    print('Black reference completed!')
    time.sleep(2)
    # Open up the shutter again
    grabber.remote.set('LensShutter','LensShutterOpen')
    return black_ref

# Subtract the black reference from a raw frame.
def subtract_black_ref(raw_frame, black_ref, daType, daTypeMax):
    # If in color, debayer the frame.
    if clrString=='BayerGB':
        raw_frame = cv2.cvtColor(raw_frame, cv2.COLOR_BayerGB2RGB)
    # If in 12 bpp mode, bit shift 4 times (since the pixel values are sent as
    # 16 bits) and convert to an int32 array. This is to prevent underflow
    # when subtracting out the black reference, as we will see in a bit.
    if whatisBit==8:
        raw_frame = raw_frame.astype(np.int16)
    else:
        raw_frame = raw_frame.astype(np.int32)
    # raw frame = raw frame - black ref
    raw_frame -= black_ref
    # This is why we initially cast the arrays as int16 or int32 -- if we were
    # to leave the data as uint8 or uint16, if raw_frame(m,n) < black_ref(m,n),
    # where m,n indexes the (col,row)th pixel, then this would underflow back
    # to the max. For example, if raw_frame(m,n)=5 and black_ref(m,n)=10, then
    # the subtraction would yield "-5", but this is an unsigned integer, so it
    # would actually underflow and wrap around to 255 and the value would 
    # register as 251 (4, 3, 2, 1, 0, 255, 254, 253, 252, 251). 
    # Thus, we cast as signed integers and just clip the frame back to the
    # proper unsigned values. Any negative values get clipped to zero.
    processed_frame = np.clip(raw_frame, 0, daTypeMax).astype(daType)
    return processed_frame

# The gamma correction. We create a look-up table (LUT) for fast calculation.
inv_gamma = 1.0 / 2.2
table = np.array([((i / daTypeMax) ** inv_gamma) * daTypeMax for i in np.arange(0, daTypeMax+1)], dtype=daType)
def add_gamma(frame):
    # Apply the lookup table.
    if whatisBit==8:
        frame = cv2.LUT(frame, table)
    else:
        frame = table[frame]
    return frame
############################################################################

########################## Grab the frame ##################################

# Get black reference
black_ref=get_black_ref(bpc)
time.sleep(2)

# Get a frame. Realloc(ate) makes sure to clear any buffers in preparation for
# new data.
grabber.realloc_buffers(numBuff)
grabber.start()
with eg.Buffer(grabber) as buffer:
    bufferPtr_raw,imageSize_raw,partNum_raw,timeStamp_raw = grab_frame_info(buffer)
    raw_frame = move_frame_from_pool(bufferPtr_raw,imageSize_raw,partNum_raw)
grabber.stop()

# Bit shift raw_frame down 4 if in 12 bit mode
if whatisBit==12:
    raw_frame=raw_frame<<4

# Subtract black reference
processed_frame_no_gamma = subtract_black_ref(raw_frame, black_ref, daType,
                                              daTypeMax)

# Gamma correction
processed_frame_with_gamma = add_gamma(processed_frame_no_gamma)

########################## Display the frame ##############################
resizeFlag=False
while True:
    dispstr1='Raw frame. Press Q to exit.'
    dispstr2='Processed frame without gamma. Press Q to exit.'
    dispstr3='Processed frame with gamma. Press Q to exit.'
    cv2.namedWindow(dispstr1, cv2.WINDOW_NORMAL)
    cv2.namedWindow(dispstr2, cv2.WINDOW_NORMAL)
    cv2.namedWindow(dispstr3, cv2.WINDOW_NORMAL)
    if resizeFlag==False:
        cv2.resizeWindow(dispstr1, w, h)
        cv2.resizeWindow(dispstr2, w, h)
        cv2.resizeWindow(dispstr3, w, h)
        resizeFlag=True
    cv2.imshow(dispstr3, processed_frame_with_gamma)
    cv2.imshow(dispstr2, processed_frame_no_gamma)
    cv2.imshow(dispstr1, raw_frame)
    key = cv2.waitKey(1) & 0xFF
    
    if key==ord('q'):
        break
    
cv2.destroyAllWindows()
